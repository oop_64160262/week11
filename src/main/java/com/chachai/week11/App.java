package com.chachai.week11;

import java.util.logging.Handler;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Bat bat1 = new Bat("Batman");
       bat1.eat();
       bat1.sleep();
       bat1.takeoff();
       bat1.fly();
       bat1.landing();

       Fish fish1 = new Fish("Thonggas");
       fish1.eat();
       fish1.sleep();

       Plane plane1 = new Plane("TukTuk", "TukTuk Engine");
       plane1.fly();
       plane1.takeoff();
       plane1.landing();

       Bird bird1 = new Bird("Nok");
       bird1.eat();
       bird1.sleep();
       bird1.fly();
       bird1.takeoff();
       bird1.landing();

       Submarine submarine1 = new Submarine("Happy", "Happy Engine");
       submarine1.swim();

       Crocodile crocodile1 = new Crocodile("Kai");
       crocodile1.eat();
       crocodile1.sleep();
       crocodile1.crawl();
       crocodile1.swim();

       Snake snake1 = new Snake("Nong");
       snake1.eat();
       snake1.sleep();
       snake1.crawl();

       Human human1 = new Human("Praew");
       human1.eat();
       human1.sleep();
       human1.walk();
       human1.run();

       Rat rat1 = new Rat("Faichaek");
       rat1.eat();
       rat1.sleep();
       rat1.walk();
       rat1.run();

       Cat cat1 = new Cat("Tao");
       cat1.eat();
       cat1.sleep();
       cat1.walk();
       cat1.run();

       Dog dog1 = new Dog("Haribo");
       dog1.eat();
       dog1.sleep();
       dog1.walk();
       dog1.run();

       Flyable[] flyablesObjects = {bat1,plane1,bird1};
       for(int i = 0; i < flyablesObjects.length; i++ ){
           flyablesObjects[i].takeoff();
           flyablesObjects[i].fly();
           flyablesObjects[i].landing();
       }

       Swimable[] swimablesObjects = {fish1,submarine1,crocodile1};
       for (int i = 0; i < swimablesObjects.length; i++) {
           swimablesObjects[i].swim();
       }

       Crawlable[] crawlablesObjects = {crocodile1,snake1};
       for (int i = 0; i < crawlablesObjects.length; i++) {
           crawlablesObjects[i].crawl();
       }

       Walkable[] walkablesObjects = {human1,rat1,cat1,dog1};
       for (int i = 0; i < walkablesObjects.length; i++) {
           walkablesObjects[i].walk();
           walkablesObjects[i].run();
       }
    } 
}
